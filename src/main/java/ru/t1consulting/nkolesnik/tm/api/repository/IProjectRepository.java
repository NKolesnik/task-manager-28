package ru.t1consulting.nkolesnik.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, String name, @NotNull String description);

}
