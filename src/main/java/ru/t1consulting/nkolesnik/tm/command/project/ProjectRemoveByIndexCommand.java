package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-index";

    @NotNull
    public static final String DESCRIPTION = "Remove project by index with tasks by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = getProjectService().findByIndex(index);
        @NotNull final String userId = getUserId();
        if (project == null) return;
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
